package com.lcj.dao;

import com.lcj.model.Picture;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Administrator on 2017/4/9/009.
 */
@Repository
public interface PictureDao {
    @Insert("INSERT INTO `t_mz_pic` (`picinfo`, `url`, `path`, `create_time`, `update_time`) VALUES (#{picinfo}, #{url}, #{path},NOW(),NOW())")
    int insert(Picture record);

    @Results(
            {@Result(property = "createTime",column = "create_time"),
            @Result(property = "updateTime",column = "update_time")}
    )
    @Select("SELECT * FROM t_mz_pic LIMIT #{limit}  OFFSET #{start}")
    List<Picture> selectPages(@Param("start") int start, @Param("limit") int limit);
}
