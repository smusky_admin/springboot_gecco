package com.lcj.dao;

import com.lcj.model.CategoryType;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Administrator on 2017/4/9/009.
 */
@Repository
public interface CategoryTypeDao {

    @Insert("INSERT INTO `t_mz_tag` (`tag_name`, `url`, `tag_pinyin`, `tag_code`, `create_time`, `update_time`) VALUES (#{tagName}, #{url}, #{tagPinyin}, #{tagCode}, NOW(), NOW())")
    int insert(CategoryType record);

    @Select("SELECT * FROM tb_mz_tag")
    List<CategoryType> findAll();
}
