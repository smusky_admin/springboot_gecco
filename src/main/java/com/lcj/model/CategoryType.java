package com.lcj.model;

import com.geccocrawler.gecco.annotation.Href;
import com.geccocrawler.gecco.annotation.HtmlField;
import com.geccocrawler.gecco.annotation.Text;
import com.geccocrawler.gecco.spider.HtmlBean;

import java.util.Date;

public class CategoryType implements HtmlBean {

    private Integer id;

    @Text
    @HtmlField(cssPath = "a")
    private String tagName;

    @Href
    @HtmlField(cssPath = "a")
    private String tagPinyin;

    @Href
    @HtmlField(cssPath = "a")
    private String tagCode;

    @Href
    @HtmlField(cssPath = "a")
    private String url;

    private Date createTime;

    private Date updateTime;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public String getTagPinyin() {
        return tagPinyin;
    }

    public void setTagPinyin(String tagPinyin) {
        this.tagPinyin = processUrl(tagPinyin)[0];
    }

    public String getTagCode() {
        return tagCode;
    }

    public void setTagCode(String tagCode) {
        this.tagCode = processUrl(tagCode)[1];
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "CategoryType{" +
                "id=" + id +
                ", tagName='" + tagName + '\'' +
                ", tagPinyin='" + tagPinyin + '\'' +
                ", tagCode='" + tagCode + '\'' +
                ", url='" + url + '\'' +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }

    private  String[] processUrl(String url){
        //http://www.meizitu.com/tag/suxiong_17_1.html
        int first = url.lastIndexOf("/");
        int last = url.lastIndexOf("_");
        String[] result = url.substring(first + 1,last).split("_");
        return result;
    }

    public static void main(String[] args) {
        String url = "http://www.meizitu.com/tag/suxiong_17_1.html";
        int first = url.lastIndexOf("/");
        int last = url.lastIndexOf("_");
        String[] result = url.substring(first + 1,last).split("_");
        System.out.println(result[0] + ":" + result[1]);
    }
}