package com.lcj.crawler;

import com.geccocrawler.gecco.GeccoEngine;
import com.geccocrawler.gecco.annotation.Gecco;
import com.geccocrawler.gecco.annotation.HtmlField;
import com.geccocrawler.gecco.annotation.Image;
import com.geccocrawler.gecco.annotation.RequestParameter;
import com.geccocrawler.gecco.request.HttpGetRequest;
import com.geccocrawler.gecco.spider.HtmlBean;

import java.util.List;

/**
 * Created by Administrator on 2017/4/9/009.
 */
@Gecco(matchUrl = "http://www.meizitu.com/a/{code}.html", pipelines = { "consolePipeline", "bigPicPipeline" })
public class BigPic implements HtmlBean {

    private static final long serialVersionUID = 1L;

    @RequestParameter
    private int code;

    @Image
    @HtmlField(cssPath="#picture > p > img")
    private List<String> pics;

    public List<String> getPics() {
        return pics;
    }

    public void setPics(List<String> pics) {
        this.pics = pics;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public static void main(String[] args) {
        HttpGetRequest start = new HttpGetRequest("http://www.meizitu.com/a/375.html");
        start.setCharset("GBK");
        GeccoEngine.create().classpath("com.lcj").start(start).interval(2000).run();
    }

}
