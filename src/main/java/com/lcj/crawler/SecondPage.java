package com.lcj.crawler;

import com.geccocrawler.gecco.GeccoEngine;
import com.geccocrawler.gecco.annotation.*;
import com.geccocrawler.gecco.request.HttpGetRequest;
import com.geccocrawler.gecco.request.HttpRequest;
import com.geccocrawler.gecco.spider.HtmlBean;

import java.util.List;

/**
 * Created by Administrator on 2017/4/9/009.
 * 3,按标签抓取内容
 */
@Gecco(matchUrl = "http://www.meizitu.com/tag/{tagCode}_{tagId}_{currPage}.html", pipelines = {"consolePipeline", "nextPagePipeline"})
public class SecondPage implements HtmlBean {

    private static final long serialVersionUID = 1L;

    //标签的拼音表示
    @RequestParameter
    private String tagCode;

    //标签的名字冗余
    @RequestParameter
    private String tagName;

    //标签的id
    @RequestParameter
    private String tagId;

    @Request
    private HttpRequest request;

    //当前页
    @RequestParameter
    private int currPage;

    //最后一页
    @Href
    @HtmlField(cssPath = "#wp_page_numbers > ul > li:last-child > a")
    private String lastPageUrl;

    @HtmlField(cssPath = ".wp-list > li")
    private List<Picture> pictures;

    public List<Picture> getPictures() {
        return pictures;
    }

    public void setPictures(List<Picture> pictures) {
        this.pictures = pictures;
    }

    public HttpRequest getRequest() {
        return request;
    }

    public void setRequest(HttpRequest request) {
        this.request = request;
    }

    public int getCurrPage() {
        return currPage;
    }

    public void setCurrPage(int currPage) {
        this.currPage = currPage;
    }

    public String getLastPageUrl() {
        return lastPageUrl;
    }

    public void setLastPageUrl(String lastPageUrl) {
        this.lastPageUrl = lastPageUrl;
    }

    public String getTagCode() {
        return tagCode;
    }

    public void setTagCode(String tagCode) {
        this.tagCode = tagCode;
    }

    public String getTagId() {
        return tagId;
    }

    public void setTagId(String tagId) {
        this.tagId = tagId;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public static void main(String[] args) {
        HttpRequest request = new HttpGetRequest("http://www.meizitu.com/tag/suxiong_17_8.html");
        request.setCharset("GBK");
        GeccoEngine.create().classpath("com.lcj").start(request).interval(2000).run();
    }

}
