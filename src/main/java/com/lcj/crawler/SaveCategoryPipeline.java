package com.lcj.crawler;

import com.geccocrawler.gecco.pipeline.Pipeline;
import com.geccocrawler.gecco.request.HttpRequest;
import com.geccocrawler.gecco.scheduler.SchedulerContext;
import com.lcj.model.CategoryType;
import com.lcj.service.CategoryTypeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Administrator on 2017/4/9/009.
 * 2,保存tab到数据库,同时继续下一次抓取
 */
@Service
public class SaveCategoryPipeline implements Pipeline<IndexPage> {

    @Resource(name="categoryTypeServiceImpl")
    private CategoryTypeService categoryTypeService;

    @Override
    public void process(IndexPage bean) {
        List<CategoryType> categoryTypes = bean.getCategoryTypes();
        for (CategoryType categoryType : categoryTypes) {
            categoryTypeService.save(categoryType);
            //标签再次请求
            HttpRequest sub = bean.getRequest().subRequest(categoryType.getUrl());
            //继续爬取
            sub.addParameter("tagName",categoryType.getTagName());
            SchedulerContext.into(sub);
        }
    }
}
