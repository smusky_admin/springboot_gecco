package com.lcj.crawler;

import com.geccocrawler.gecco.annotation.Attr;
import com.geccocrawler.gecco.annotation.Href;
import com.geccocrawler.gecco.annotation.HtmlField;
import com.geccocrawler.gecco.annotation.Image;
import com.geccocrawler.gecco.spider.HtmlBean;

/**
 * Created by Administrator on 2017/4/9/009.
 * 列表页图片
 */
public class Picture implements HtmlBean {

    private static final long serialVersionUID = 1L;

    @Attr("alt")
    @HtmlField(cssPath = "div.con > .pic > a > img")
    private String alt;

    @Href
    @HtmlField(cssPath = "div.con > .pic > a")
    private String href;

    @Href
    @HtmlField(cssPath = "div.con > .pic > a")
    private String picCode;

    @Image
    @HtmlField(cssPath = "div.con > .pic > a > img")
    private String thumbnail;

    public String getAlt() {
        return alt;
    }

    public void setAlt(String alt) {
        this.alt = alt;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getPicCode() {
        return picCode;
    }

    public void setPicCode(String picCode) {
        //http://www.meizitu.com/a/4647.html
        int first = picCode.lastIndexOf("/");
        int last = picCode.lastIndexOf(".");
        this.picCode = picCode.substring(first + 1,last);
    }
}
