package com.lcj.service;

import com.lcj.model.CategoryType;

import java.util.List;

/**
 * Created by Administrator on 2017/4/9/009.
 */
public interface CategoryTypeService {
    void save(CategoryType categoryType);

    List<CategoryType> findAll();
}
