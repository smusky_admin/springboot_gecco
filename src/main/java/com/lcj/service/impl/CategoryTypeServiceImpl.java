package com.lcj.service.impl;

import com.lcj.dao.CategoryTypeDao;
import com.lcj.model.CategoryType;
import com.lcj.service.CategoryTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Administrator on 2017/4/9/009.
 */
@Service
public class CategoryTypeServiceImpl implements CategoryTypeService {

    @Autowired
    private CategoryTypeDao categoryTypeDao;

    @Override
    public void save(CategoryType categoryType) {
        categoryTypeDao.insert(categoryType);
    }

    @Override
    public List<CategoryType> findAll() {
        return categoryTypeDao.findAll();
    }
}
