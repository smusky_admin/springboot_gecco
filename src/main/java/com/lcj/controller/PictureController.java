package com.lcj.controller;

import com.lcj.model.Picture;
import com.lcj.service.PictureService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Administrator on 2017/4/9/009.
 */
@RestController
public class PictureController {
    private static final Logger LOGGER = Logger.getLogger(PictureController.class);

    @Autowired
    private PictureService pictureService;

    @RequestMapping(value = "/json/{start}/{limit}")
    public List<Picture> pictures(@PathVariable int start , @PathVariable int limit){
        LOGGER.info("参数: " + start + ":" + limit);
        return pictureService.selectPages((start - 1) * limit,limit);
    }
}
