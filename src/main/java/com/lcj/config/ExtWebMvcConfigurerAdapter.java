package com.lcj.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Created by Administrator on 2017/4/6/006.
 * 扩展spring mvc配置
 */
@Configuration
public class ExtWebMvcConfigurerAdapter extends WebMvcConfigurerAdapter {

    /*添加资源处理*/
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        //将本地目录D:\UserData\Administrator\Pictures\meinv中的图片加载到项目中,使用http://localhost/img/meinv_023.jpg访问
        registry.addResourceHandler("/meizitu/**").addResourceLocations("file:D:/meizitu/");
        super.addResourceHandlers(registry);
    }

    /*如果跳转到一个页面没有任何逻辑处理,那么可以直接在这里定义*/

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        //registry.addViewController("/forward").setViewName("forward");
        super.addViewControllers(registry);
    }

    /**
     * 拦截器:实现HandlerInterceptor 接口
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // addPathPatterns 用于添加拦截规则
        // excludePathPatterns 用户排除拦截
        //registry.addInterceptor(new MyInterceptor()).addPathPatterns("/**").excludePathPatterns("/toLogin","/login");
        super.addInterceptors(registry);
    }
}
