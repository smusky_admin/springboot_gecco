package com.lcj.config;

import com.geccocrawler.gecco.pipeline.PipelineFactory;
import com.geccocrawler.gecco.spring.SpringPipelineFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Administrator on 2017/4/9/009.
 */
@Configuration
public class PipelineFactoryConfig {

    @Bean
    public PipelineFactory springPipelineFactory(){
        return new SpringPipelineFactory();
    }
}
